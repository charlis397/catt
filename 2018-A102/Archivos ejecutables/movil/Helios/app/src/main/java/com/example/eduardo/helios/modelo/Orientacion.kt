package com.example.eduardo.helios.modelo

data class Orientacion(var puntoCardinal:String = "", var grados:Int = 0, var estacion:String = "")