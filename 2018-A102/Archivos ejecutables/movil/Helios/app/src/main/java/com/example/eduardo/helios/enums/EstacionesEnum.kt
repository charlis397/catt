package com.example.eduardo.helios.enums

enum class EstacionesEnum(val identificador:Int, val nombre:String){
    PRIMAVERA(1,"Primavera"), VERANO(2,"Verano"), OTOÑO(3,"Otoño"), INVIERNO(4,"Invierno")
}