package com.example.eduardo.helios.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.view.*
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.eduardo.helios.GestionarDispositivos
import com.example.eduardo.helios.R
import com.example.eduardo.helios.activities.EditarPanel
import com.example.eduardo.helios.activities.RegistrarPanel
import com.example.eduardo.helios.enums.OperacionesEnum
import com.example.eduardo.helios.modelo.Panel
import kotlinx.android.synthetic.main.fragment_gestionar_paneles.*
import kotlinx.android.synthetic.main.fragment_gestionar_paneles.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * Clase que contiene el fragmento de los paneles
 */
class GestionarPaneles : Fragment(), Response.Listener<JSONObject>, Response.ErrorListener, SwipeRefreshLayout.OnRefreshListener {

    var panelSeleccionado = Panel()

    var ip:String = ""

    override fun onRefresh() {
        obtenerPaneles()
        refreshGestionarPaneles.isRefreshing = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_gestionar_paneles, container, false)
        rootView.refreshGestionarPaneles.setOnRefreshListener(this)
        fun agregarPanel() = startActivity(Intent(activity, RegistrarPanel::class.java))

        val fin = BufferedReader(
                InputStreamReader(activity!!.openFileInput("ip.txt")))
        ip = fin.readLine()
        fin.close()

        obtenerPaneles()

        rootView.addPanel.setOnClickListener { agregarPanel() }

        return rootView
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)

        if (menu != null) {
            menu.add(Menu.NONE, R.id.editarPanelMC, Menu.NONE,"Editar")
            menu.add(Menu.NONE, R.id.eliminarPanelMC, Menu.NONE,"Eliminar")
        }



    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.editarPanelMC -> {
                val intent = Intent(activity, EditarPanel::class.java)
                intent.putExtra("id_panel_sel",panelSeleccionado.id)
                startActivity(intent)
                return true
            }
            R.id.eliminarPanelMC->{
                val builder = AlertDialog.Builder(context)

                builder.setTitle("Eliminar panel fotovoltaico")
                builder.setMessage("¿Está seguro que desea eliminar el panel fotovoltaico?")
                builder.setPositiveButton("Sí"){dialog, which ->

                    val url = "http://$ip/eliminarPanel.php?id_panel=${panelSeleccionado.id}"
                    val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
                    val rq = Volley.newRequestQueue(context)

                    rq.add(jrq)
                }


                builder.setNegativeButton("No"){dialog,which ->

                }


                val dialog: AlertDialog = builder.create()
                dialog.show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
            }
    }

    companion object {

        fun newInstance(): GestionarPaneles {
            return GestionarPaneles()
        }
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResponse(response: JSONObject) {

        val listaPaneles: ArrayList<Panel> = ArrayList()

        val jsonArray: JSONArray = response.optJSONArray("datos")


        var longitud = jsonArray.length()

        var tipoRespuestaJson = jsonArray.getJSONObject(0)
        val tipoRespuesta = tipoRespuestaJson.optInt("operacion")

        if(tipoRespuesta == OperacionesEnum.CONSULTAR.identificador){
        for (i in 1 until longitud) {

            var jsonObject = jsonArray.getJSONObject(i)

            var panel: Panel = Panel()

            panel.id = jsonObject.optString("id_panel").toInt()
            panel.nombre = jsonObject.optString("nombre")
            panel.voltajeMax = jsonObject.optString("voltaje_max").toDouble()
            panel.corrienteMax = jsonObject.optString("corriente_max").toDouble()

            listaPaneles.add(panel)

        }

            tablePaneles.removeAllViews()
            pintarCabezeraTabla()

        listaPaneles.forEach {
            val tableRow: TableRow = TableRow(context)
            val nombre: TextView = TextView(context)
            val voltajeMax: TextView = TextView(context)
            val corrienteMax: TextView = TextView(context)

            nombre.text = it.nombre
            voltajeMax.text = it.voltajeMax.toString()
            corrienteMax.text = it.corrienteMax.toString()

            nombre.setTextAppearance(R.style.TextAppearance_AppCompat_Body1)
            voltajeMax.setTextAppearance(R.style.TextAppearance_AppCompat_Body1)
            corrienteMax.setTextAppearance(R.style.TextAppearance_AppCompat_Body1)

            voltajeMax.gravity = Gravity.CENTER
            corrienteMax.gravity = Gravity.CENTER

            tableRow.addView(nombre)
            tableRow.addView(voltajeMax)
            tableRow.addView(corrienteMax)

            val tableRowParams = TableLayout.LayoutParams()
            tableRowParams.setMargins(0, 4, 0, 4)
            tableRow.layoutParams = tableRowParams

            val attrs = intArrayOf(R.attr.selectableItemBackground)
            val typedArray = context!!.obtainStyledAttributes(attrs)
            val backgroundResource = typedArray.getResourceId(0, 0)

            tableRow.setBackgroundResource(backgroundResource)
            typedArray.recycle()
            tableRow.isLongClickable = true
            tableRow.isClickable = true
            registerForContextMenu(tableRow)
            tablePaneles.addView(tableRow)

            tableRow.setOnCreateContextMenuListener { menu, v, menuInfo ->
                super.onCreateContextMenu(menu, v, menuInfo)

                if (menu != null) {
                    menu.add(Menu.NONE, R.id.editarPanelMC, Menu.NONE, "Editar")
                    menu.add(Menu.NONE, R.id.eliminarPanelMC, Menu.NONE, "Eliminar")

                    panelSeleccionado.id = it.id
                }
            }

        }
        }else if (tipoRespuesta == OperacionesEnum.ELIMINAR.identificador){
            Toast.makeText(context,"El panel se eliminó exitosamente", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Funcion que permite obtener todos aquellos paneles que no tienen un eliminado logico
     */
    private fun obtenerPaneles(){
        val url = "http://$ip/panelDAO.php"
        val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
        val rq = Volley.newRequestQueue(context)
        rq.add(jrq)
    }

    /**
     * Funcion que permite pintar la cabezera de la tabla
     */
    private fun pintarCabezeraTabla(){
        val tableRowTitle: TableRow = TableRow(context)
        val nombreTitle: TextView = TextView(context)
        val voltajeMinimoTitle: TextView = TextView(context)
        val celdasTitle: TextView = TextView(context)

        nombreTitle.text = getString(R.string.IUF1_1_THEAD1)
        voltajeMinimoTitle.text = getString(R.string.IUF1_1_THEAD2)
        celdasTitle.text = getString(R.string.IUF1_1_THEAD3)

        nombreTitle.setTextAppearance(R.style.TextAppearance_AppCompat_Body2)
        voltajeMinimoTitle.setTextAppearance(R.style.TextAppearance_AppCompat_Body2)
        celdasTitle.setTextAppearance(R.style.TextAppearance_AppCompat_Body2)

        nombreTitle.setTextColor(resources.getColor(R.color.botonText))
        voltajeMinimoTitle.setTextColor(resources.getColor(R.color.botonText))
        celdasTitle.setTextColor(resources.getColor(R.color.botonText))

        nombreTitle.gravity = Gravity.CENTER
        voltajeMinimoTitle.gravity = Gravity.CENTER


        tableRowTitle.addView(nombreTitle)
        tableRowTitle.addView(voltajeMinimoTitle)
        tableRowTitle.addView(celdasTitle)

        tableRowTitle.setBackgroundColor(resources.getColor(R.color.colorAccent))

        tablePaneles.addView(tableRowTitle)
    }


    override fun onErrorResponse(error: VolleyError?) {
        startActivity(Intent(context, GestionarDispositivos::class.java))
        Toast.makeText(context,"El panel se eliminó exitosamente", Toast.LENGTH_LONG).show()
    }



}