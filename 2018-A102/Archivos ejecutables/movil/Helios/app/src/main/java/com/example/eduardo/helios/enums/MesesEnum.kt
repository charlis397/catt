package com.example.eduardo.helios.enums

enum class MesesEnum(val numeroMes:Int, val nombre:String){
ENERO(1,"Enero"), FEBRERO(2,"Febrero"), MARZO(3,"Marzo"), ABRIL(4,"Abril"), MAYO(5,"Mayo"), JUNIO(6,"Junio")
    , JULIO(7,"Julio"), AGOSTO(8,"Agosto"), SEPTIEMBRE(9,"Septiembre"), OCTUBRE(10,"Octubre"), NOVIEMBRE(11,"Noviembre"), DICIEMBRE(12,"Diciembre")
}