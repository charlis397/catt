package com.example.eduardo.helios.modelo

data class Panel(var id:Int = 0, var idMecanismo:Int = 0, var nombre:String ="", var voltajeMax:Double=0.0, var corrienteMax:Double=0.0)
