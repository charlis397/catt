package com.example.eduardo.helios.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.util.Log
import android.view.*
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.eduardo.helios.GestionarDispositivos
import com.example.eduardo.helios.R
import com.example.eduardo.helios.activities.AgregarBateria
import com.example.eduardo.helios.activities.EditarBateria
import com.example.eduardo.helios.enums.OperacionesEnum
import com.example.eduardo.helios.modelo.Bateria
import com.example.eduardo.helios.utileria.BooleanUtil
import kotlinx.android.synthetic.main.fragment_gestionar_bateria.*
import kotlinx.android.synthetic.main.fragment_gestionar_bateria.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader


class GestionarBaterias : Fragment(), Response.Listener<JSONObject>, Response.ErrorListener, SwipeRefreshLayout.OnRefreshListener {


    var listTableRow = ArrayList<TableRow>()
    var bateriaSeleccionada = Bateria()

    var ip = ""


    override fun onRefresh() {
        obtenerBaterias()
        refreshGestionarBaterias.isRefreshing = false
    }

    override fun onErrorResponse(error: VolleyError?) {
        startActivity(Intent(context, GestionarDispositivos::class.java))
        Toast.makeText(context,"La batería se eliminó correctamente", Toast.LENGTH_LONG).show()
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResponse(response: JSONObject) {


        var listaBaterias: ArrayList<Bateria> = ArrayList()


        val jsonArray: JSONArray = response.optJSONArray("datos")
        var tipoRespuestaJson = jsonArray.getJSONObject(0)
        val tipoRespuesta = tipoRespuestaJson.optInt("operacion")

        if(tipoRespuesta == OperacionesEnum.CONSULTAR.identificador){



            var longitud = jsonArray.length()


        for (i in 1 until longitud) {

            var jsonObject = jsonArray.getJSONObject(i)

            var bateria: Bateria = Bateria()

            bateria.id = jsonObject.optInt("id_bateria")
            bateria.nombre = jsonObject.optString("nombre")
            bateria.voltajeMax = jsonObject.optDouble("voltaje_max")
            bateria.voltajeMin = jsonObject.optDouble("voltaje_min")
            bateria.corriente = jsonObject.optDouble("corriente")
            bateria.numeroCeldas = jsonObject.optInt("nu_celdas")
            bateria.temperatura = jsonObject.optDouble("temperatura_max")
            bateria.hasMemoria = jsonObject.optBoolean("hasMemoria")
            bateria.isEliminado = jsonObject.optBoolean("isEliminado")
            bateria.isActivo = BooleanUtil.toBoolean(jsonObject.optInt("activo"))


            listaBaterias.add(bateria)

        }

            tableBaterias.removeAllViews()
            pintarCabezeraTabla()



        listaBaterias.forEach {
            val tableRow: TableRow = TableRow(context)
            val nombre: TextView = TextView(context)
            val voltajeMinimo: TextView = TextView(context)
            val celdas: TextView = TextView(context)

            nombre.text = it.nombre
            voltajeMinimo.text = it.voltajeMin.toString()
            celdas.text = it.numeroCeldas.toString()

            nombre.setTextAppearance(R.style.TextAppearance_AppCompat_Body1)
            voltajeMinimo.setTextAppearance(R.style.TextAppearance_AppCompat_Body1)
            celdas.setTextAppearance(R.style.TextAppearance_AppCompat_Body1)

            val isActivo = it.isActivo

            if(isActivo != null && isActivo){
                Log.w("ENTRE" , "ENTRE")
                nombre.setTextColor(resources.getColor(R.color.colorAccent))
                voltajeMinimo.setTextColor(resources.getColor(R.color.colorAccent))
                celdas.setTextColor(resources.getColor(R.color.colorAccent))
            }

            voltajeMinimo.gravity = Gravity.CENTER
            celdas.gravity = Gravity.CENTER



            tableRow.addView(nombre)
            tableRow.addView(voltajeMinimo)
            tableRow.addView(celdas)



            val tableRowParams = TableLayout.LayoutParams()
            tableRowParams.setMargins(0, 4, 0, 4)
            tableRow.layoutParams = tableRowParams

            val attrs = intArrayOf(R.attr.selectableItemBackground)
            val typedArray = context!!.obtainStyledAttributes(attrs)
            val backgroundResource = typedArray.getResourceId(0, 0)

            tableRow.setBackgroundResource(backgroundResource)
            typedArray.recycle()
            tableRow.isLongClickable = true
            tableRow.isClickable = true
            registerForContextMenu(tableRow)
            tableBaterias.addView(tableRow)

            tableRow.setOnCreateContextMenuListener { menu, v, menuInfo ->
                super.onCreateContextMenu(menu, v, menuInfo)

                if (menu != null) {
                    menu.add(Menu.NONE, R.id.editarBateriaMC, Menu.NONE,"Editar")

                    if(!it.isActivo!!){
                        menu.add(Menu.NONE, R.id.eliminarBateriaMC, Menu.NONE,"Eliminar")
                        menu.add(Menu.NONE, R.id.priorizarBateriaMC, Menu.NONE,"Priorizar")
                    }


                    bateriaSeleccionada.id = it.id
                }
            }

        }
        }else if (tipoRespuesta == OperacionesEnum.ELIMINAR.identificador){
            Toast.makeText(context,"La batería se eliminó correctamente", Toast.LENGTH_LONG).show()
        }else if(tipoRespuesta == OperacionesEnum.PRIORIDAD.identificador){
            Toast.makeText(context,"La batería se priorizó correctamente", Toast.LENGTH_LONG).show()
        }

        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_gestionar_bateria, container, false)
        rootView.refreshGestionarBaterias.setOnRefreshListener(this)
        val fin = BufferedReader(
                InputStreamReader(activity!!.openFileInput("ip.txt")))
        ip = fin.readLine()
        fin.close()

        fun aregarBateria() = startActivity(Intent(activity, AgregarBateria::class.java))

        obtenerBaterias()
        rootView.addBateria.setOnClickListener { aregarBateria() }

        return rootView
    }

    /**
     * Funcion que permite obtener todas aquellas baterias que no se encuentran eliminadas
     */
    private fun obtenerBaterias(){
        val url = "http://$ip/bateriaDAO.php"
        val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
        val rq = Volley.newRequestQueue(context)
        rq.add(jrq)
    }

    /**
     * Funcion que permite pintar la cabezera de la tabla
     */
    private fun pintarCabezeraTabla(){
        val tableRowTitle: TableRow = TableRow(context)
        val nombreTitle: TextView = TextView(context)
        val voltajeMinimoTitle: TextView = TextView(context)
        val celdasTitle: TextView = TextView(context)

        nombreTitle.text = getString(R.string.IUF1_1_THEAD5)
        voltajeMinimoTitle.text = getString(R.string.IUF1_1_THEAD6)
        celdasTitle.text = getString(R.string.IUF1_1_THEAD7)

        nombreTitle.setTextAppearance(R.style.TextAppearance_AppCompat_Body2)
        voltajeMinimoTitle.setTextAppearance(R.style.TextAppearance_AppCompat_Body2)
        celdasTitle.setTextAppearance(R.style.TextAppearance_AppCompat_Body2)

        nombreTitle.setTextColor(resources.getColor(R.color.botonText))
        voltajeMinimoTitle.setTextColor(resources.getColor(R.color.botonText))
        celdasTitle.setTextColor(resources.getColor(R.color.botonText))

        nombreTitle.gravity = Gravity.CENTER
        voltajeMinimoTitle.gravity = Gravity.CENTER


        tableRowTitle.addView(nombreTitle)
        tableRowTitle.addView(voltajeMinimoTitle)
        tableRowTitle.addView(celdasTitle)

        tableRowTitle.setBackgroundColor(resources.getColor(R.color.colorAccent))

        tableBaterias.addView(tableRowTitle)
    }



    override fun onContextItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.editarBateriaMC->{
                val intent = Intent(activity, EditarBateria::class.java)
                intent.putExtra("id_bateria_sel",bateriaSeleccionada.id)
                startActivity(intent)
                return true
            }
            R.id.eliminarBateriaMC->{
                val builder = AlertDialog.Builder(context)

                builder.setTitle("Eliminar batería")
                builder.setMessage("¿Está seguro que desea eliminar la batería?")
                builder.setPositiveButton("Sí"){dialog, which ->

                    val url = "http://$ip/eliminarBateria.php?id_bateria=${bateriaSeleccionada.id}"
                    val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
                    val rq = Volley.newRequestQueue(context)

                    rq.add(jrq)

                }


                builder.setNegativeButton("No"){dialog,which ->

                }


                val dialog: AlertDialog = builder.create()
                dialog.show()
                return true
            }
            R.id.priorizarBateriaMC->{
                val builder = AlertDialog.Builder(context)

                builder.setTitle("Priorizar batería")
                builder.setMessage("¿Está seguro que desea emplear esta batería?")
                builder.setPositiveButton("Sí"){dialog, which ->

                    val url = "http://$ip/prioridad.php?id_bateria=${bateriaSeleccionada.id}"
                    val jrq = JsonObjectRequest(Request.Method.GET,url,null,this,this)
                    val rq = Volley.newRequestQueue(context)

                    rq.add(jrq)

                }


                builder.setNegativeButton("No"){dialog,which ->

                }


                val dialog: AlertDialog = builder.create()
                dialog.show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {

        fun newInstance(): GestionarBaterias {
            return  GestionarBaterias()
        }
    }

}

