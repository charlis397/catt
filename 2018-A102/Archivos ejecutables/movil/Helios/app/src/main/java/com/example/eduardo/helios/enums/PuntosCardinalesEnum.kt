package com.example.eduardo.helios.enums

enum class PuntosCardinalesEnum(val identificador:Int, val nombre:String){
    NORTE(1,"Norte"), SUR(2,"Sur"), ESTE(3,"Este"), OESTE(4,"Oeste")
}